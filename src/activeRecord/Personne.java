package activeRecord;


import java.sql.*;
import java.util.*;

public class Personne {
	
	private int id;
	private String nom;
	private String prenom;
	
	/**
	 * @param nom
	 * @param prenom
	 */
	public Personne(String nom, String prenom) {
		this.nom = nom;
		this.prenom = prenom;
		this.id=-1;
	}
	
	/**
	 * @return
	 */
	public int getId() {
		return id;
	}
	/**
	 * @param id
	 */
	public void setId(int id) {
		this.id = id;
	}
	/**
	 * @return
	 */
	public String getNom() {
		return nom;
	}
	/**
	 * @param nom
	 */
	public void setNom(String nom) {
		this.nom = nom;
	}
	/**
	 * @return
	 */
	public String getPrenom() {
		return prenom;
	}
	/**
	 * @param prenom
	 */
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	
	
	/**
	 * @return
	 * @throws SQLException
	 */
	public static ArrayList<Personne> findAll() throws SQLException{
		String SQLPrep = "SELECT * FROM Personne;";
		Connection connect = DBConnection.getInstance().getConnect();
		PreparedStatement prep1 = connect.prepareStatement(SQLPrep);
		prep1.execute();
		ResultSet rs = prep1.getResultSet();
		ArrayList<Personne> list=new ArrayList<Personne>();
		while (rs.next()) {
			String nom = rs.getString("nom");
			String prenom = rs.getString("prenom");
			int id = rs.getInt("id");
			
			Personne p= new Personne(nom,prenom);
			p.setId(id);
			list.add(p);
			
		}

		
		return list;
	}
	
	/**
	 * @param paraId
	 * @return
	 * @throws SQLException
	 */
	public static Personne findById(int paraId) throws SQLException{
		String SQLPrep ="SELECT * FROM Personne;";
		Connection connect = DBConnection.getInstance().getConnect();
		PreparedStatement prep1 = connect.prepareStatement(SQLPrep);
		prep1.execute();
		ResultSet rs = prep1.getResultSet();
		while (rs.next()) {
			
			int id = rs.getInt("id");
			if(id==paraId) {
				String nom = rs.getString("nom");
				String prenom = rs.getString("prenom");
				Personne p= new Personne(nom,prenom);
				p.setId(id);
				return p;
			}
			
		}
		
		return null;
	}
	/**
	 * @param paraNom
	 * @return
	 * @throws SQLException
	 */
	public static ArrayList<Personne> findByName(String paraNom) throws SQLException{
		String SQLPrep = "SELECT * FROM Personne;";
		Connection connect = DBConnection.getInstance().getConnect();
		PreparedStatement prep1 = connect.prepareStatement(SQLPrep);
		prep1.execute();
		ResultSet rs = prep1.getResultSet();
		ArrayList<Personne> list=new ArrayList<Personne>();
		while (rs.next()) {
			String nom = rs.getString("nom");
			if(nom.equals(paraNom)) {
				String prenom = rs.getString("prenom");
				int id = rs.getInt("id");
				
				Personne p= new Personne(nom,prenom);
				p.setId(id);
				list.add(p);	
			}	
		}

		return list;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Personne [id=" + id + ", nom=" + nom + ", prenom=" + prenom 
				+ "]\n";
	}
	/**
	 * @param requete
	 * @return
	 * @throws SQLException
	 */
	public ResultSet connexion(String requete) throws SQLException {
		Connection connect = DBConnection.getInstance().getConnect();
		PreparedStatement prep1 = connect.prepareStatement(requete);
		prep1.execute();
		ResultSet rs = prep1.getResultSet();
		return rs;
	}
	/**
	 * @throws SQLException
	 */
	public static void createTable() throws SQLException {
		Connection connect=  DBConnection.getInstance().getConnect();
		String createString = "CREATE TABLE Personne ( " + "ID INTEGER NOT NULL  AUTO_INCREMENT, "
							+ "NOM varchar(40) NOT NULL, " + "PRENOM varchar(40) NOT NULL, " + "PRIMARY KEY (ID))";
		Statement stmt = connect.createStatement();
		stmt.executeUpdate(createString);

	}
	/**
	 * @throws SQLException
	 */
	public static void deleteTable() throws SQLException {
		Connection connect=  DBConnection.getInstance().getConnect();
		String drop = "DROP TABLE Personne";
		Statement stmt = connect.createStatement();
		stmt.executeUpdate(drop);
	}
	/**
	 * @throws SQLException
	 */
	public  void delete() throws SQLException{
		Connection connect=  DBConnection.getInstance().getConnect();
		PreparedStatement prep = connect.prepareStatement("DELETE FROM Personne WHERE id=?");
		prep.setInt(1,this.getId() );
		prep.execute();
		this.setId(-1);
	}
	/**
	 * @throws SQLException
	 */
	public  void save() throws SQLException{	
		if(this.getId()==-1) {
			this.saveNew();
		}else {
			this.update();
		}
	}
	/**
	 * @throws SQLException
	 */
	private void saveNew() throws SQLException{
		Connection connect=  DBConnection.getInstance().getConnect();
		String SQLPrep = "INSERT INTO Personne (nom, prenom) VALUES (?,?);";
		PreparedStatement prep;
		prep = connect.prepareStatement(SQLPrep, Statement.RETURN_GENERATED_KEYS);
		prep.setString(1, this.getNom());
		prep.setString(2, this.getPrenom());
		prep.executeUpdate();
		SQLPrep ="SELECT * FROM Personne;";
		connect = DBConnection.getInstance().getConnect();
		PreparedStatement prep1 = connect.prepareStatement(SQLPrep);
		prep1.execute();
		ResultSet rs = prep1.getResultSet();
		while (rs.next()) {
			String nom = rs.getString("nom");
			String prenom = rs.getString("prenom");	
			if(nom.equals(this.getNom())&prenom.equals(this.getPrenom())) {
				int id = rs.getInt("id");
				this.setId(id);
			}
			
		}
	}
	/**
	 * @throws SQLException
	 */
	private void update() throws SQLException{
		Connection connect=  DBConnection.getInstance().getConnect();
		String SQLprep = "update Personne set nom=?, prenom=? where id=?;";
		PreparedStatement prep = connect.prepareStatement(SQLprep);
		prep.setString(1, this.getNom());
		prep.setString(2, this.getPrenom());
		prep.setInt(3, this.getId());
		prep.execute();
	}
	
	

}
