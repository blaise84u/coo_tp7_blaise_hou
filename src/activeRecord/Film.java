package activeRecord;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 * @author blaise84u
 *
 */
public class Film {
	private String titre;
	private int id;
	private int id_real;

	/**
	 * @param t
	 * @param realisateur
	 */
	public Film(String t, Personne realisateur) {
		this.titre = t;
		this.id_real = realisateur.getId();
		this.id = -1;
	}

	/**
	 * @param paraid
	 * @param paraidreal
	 * @param t
	 */
	private Film(int paraid, int paraidreal, String t) {
		this.id = paraid;
		this.id_real = paraidreal;
		this.titre = t;
	}
	/**
	 * @return
	 * @throws SQLException
	 */
	public static List<Film> findAll() throws SQLException{
		String SQLPrep = "SELECT * FROM Film;";
		Connection connect = DBConnection.getInstance().getConnect();
		PreparedStatement prep1 = connect.prepareStatement(SQLPrep);
		prep1.execute();
		ResultSet rs = prep1.getResultSet();
		List<Film> list=new ArrayList<Film>();
		while (rs.next()) {
			String nom = rs.getString("titre");
			int id_real=rs.getInt("id_rea");
			int id = rs.getInt("id");
			
			Film f= new Film(id,id_real,nom);
			list.add(f);
			
		}

		
		return list;
	}
	/**
	 * @param paraId
	 * @return
	 * @throws SQLException
	 */
	public static Film findById(int paraId) throws SQLException {
		String SQLPrep = "SELECT * FROM Film;";
		Connection connect = DBConnection.getInstance().getConnect();
		PreparedStatement prep1 = connect.prepareStatement(SQLPrep);
		prep1.execute();
		ResultSet rs = prep1.getResultSet();
		while (rs.next()) {

			int id = rs.getInt("id");
			if (id == paraId) {
				int id_real = rs.getInt("id_rea");
				String title = rs.getString("titre");
				Film p = new Film(id, id_real, title);
				return p;
			}

		}

		return null;
	}
	/**
	 * @throws SQLException
	 */
	public static void createTable() throws SQLException {
		Connection connect=  DBConnection.getInstance().getConnect();
		String createString = "CREATE TABLE IF NOT EXISTS `film` (\r\n" + 
				"  `ID` int(11) NOT NULL AUTO_INCREMENT,\r\n" + 
				"  `TITRE` varchar(40) NOT NULL,\r\n" + 
				"  `ID_REA` int(11) DEFAULT NULL,\r\n" + 
				"  PRIMARY KEY (`ID`),\r\n" + 
				"  KEY `ID_REA` (`ID_REA`)\r\n" + 
				") ";
		Statement stmt = connect.createStatement();
		stmt.executeUpdate(createString);
		createString = "ALTER TABLE `film`\r\n" + 
				"  ADD CONSTRAINT `film_ibfk_1` FOREIGN KEY (`ID_REA`) REFERENCES `personne` (`ID`) ";
		stmt = connect.createStatement();
		stmt.executeUpdate(createString);

	}
	/**
	 * @throws SQLException
	 */
	public static void deleteTable() throws SQLException {
		Connection connect=  DBConnection.getInstance().getConnect();
		String drop = "DROP TABLE Film";
		Statement stmt = connect.createStatement();
		stmt.executeUpdate(drop);
	}
	/**
	 * @return
	 * @throws SQLException
	 */
	public Personne getRealisateur() throws SQLException {
		return Personne.findById(id_real);
	}
	/**
	 * @throws SQLException
	 */
	public  void delete() throws SQLException{
		Connection connect=  DBConnection.getInstance().getConnect();
		PreparedStatement prep = connect.prepareStatement("DELETE FROM Film WHERE id=?");
		prep.setInt(1,this.getId() );
		prep.execute();
		this.setId(-1);
	}
	/**
	 * @throws SQLException
	 * @throws RealisateurAbsentException
	 */
	public  void save() throws SQLException, RealisateurAbsentException{	
		if(this.getId()==-1) {
			this.saveNew();
		}else {
			this.update();
		}
	}
	/**
	 * @throws SQLException
	 * @throws RealisateurAbsentException
	 */
	private void saveNew() throws SQLException, RealisateurAbsentException {
		if (id_real == -1) {
			throw new RealisateurAbsentException();
		} else {
			Connection connect = DBConnection.getInstance().getConnect();
			String SQLPrep = "INSERT INTO Film (titre,id_rea) VALUES (?,?);";
			PreparedStatement prep;
			prep = connect.prepareStatement(SQLPrep, Statement.RETURN_GENERATED_KEYS);
			prep.setString(1, titre);
			prep.setInt(2, id_real);
			prep.executeUpdate();
		}
	}
	
	/**
	 * @throws SQLException
	 */
	private void update() throws SQLException{
		Connection connect=  DBConnection.getInstance().getConnect();
		String SQLprep = "update Film set titre=?, id_rea=? where id=?;";
		PreparedStatement prep = connect.prepareStatement(SQLprep);
		prep.setString(1, this.getTitre());
		prep.setInt(2, this.getId_real());
		prep.setInt(3, this.getId());
		prep.execute();
	}
	/**
	 * @param p
	 * @return
	 * @throws SQLException
	 */
	public static List<Film> findByRealisateur(Personne p) throws SQLException{
		String SQLPrep = "SELECT * FROM Film;";
		Connection connect = DBConnection.getInstance().getConnect();
		PreparedStatement prep1 = connect.prepareStatement(SQLPrep);
		prep1.execute();
		ResultSet rs = prep1.getResultSet();
		List<Film> list=new ArrayList<Film>();
		while (rs.next()) {
			int idreal=p.getId();
			int id_real=rs.getInt("id_rea");
			if(idreal==id_real) {
				String titre = rs.getString("titre");
				int id = rs.getInt("id");
				
				Film f= new Film(id,idreal,titre);
				list.add(f);	
			}	
		}

		
		return list;
	}

	/**
	 * @return
	 */
	public String getTitre() {
		return titre;
	}

	/**
	 * @param titre
	 */
	public void setTitre(String titre) {
		this.titre = titre;
	}

	/**
	 * @return
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return
	 */
	public int getId_real() {
		return id_real;
	}

	/**
	 * @param id_real
	 */
	public void setId_real(int id_real) {
		this.id_real = id_real;
	}
	

}
