package activeRecord;

@SuppressWarnings("serial")
public class RealisateurAbsentException extends Exception {
	public RealisateurAbsentException() {
		super();
	}
	public RealisateurAbsentException(String s) {
		super(s);
	}
}
