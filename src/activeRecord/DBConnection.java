package activeRecord;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class DBConnection {

	private Connection connect;
	private static DBConnection instance;
	private static String dbName = "testPersonne";
	
	/**
	 * @throws SQLException
	 */
	private DBConnection() throws SQLException {
		String userName = "root";
		String password = "";
		String serverName = "localhost";
		String portNumber = "3306";
		Properties connectionProps = new Properties();
		connectionProps.put("user", userName);
		connectionProps.put("password", password);
		String urlDB = "jdbc:mysql://" + serverName + ":";
		urlDB += portNumber + "/" + DBConnection.dbName;
		connect = DriverManager.getConnection(urlDB, connectionProps);
	}
	
	/**
	 * @return
	 * @throws SQLException
	 */
	public static synchronized DBConnection getInstance() throws SQLException {
		if(instance == null) instance= new DBConnection();
		return instance;
		
	}
	/**
	 * @return
	 * @throws SQLException
	 */
	public Connection getConnect() throws SQLException {
		return this.connect;
		
	}
	
	/**
	 * @param nomDB
	 */
	public static void setNomDB(String nomDB) {
		dbName=nomDB;
		instance=null;
	}
}
