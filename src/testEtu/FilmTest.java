package testEtu;

import static org.junit.Assert.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import activeRecord.DBConnection;
import activeRecord.Film;
import activeRecord.Personne;
import activeRecord.RealisateurAbsentException;

public class FilmTest {

	/**
	 * Initialisation temporaire dans la base de donn�es pour effectuer des testes
	 * @throws SQLException
	 * @throws RealisateurAbsentException
	 */
	@Before
	public void init() throws SQLException, RealisateurAbsentException {
		Personne.createTable();
		
		Personne p=new Personne("SpielBeg","Steven");
		p.save();
		
		p=new Personne("Scott","Ridley");
		p.save();
		
		p=new Personne("Kubrick","Stanley");
		p.save();
		
		p=new Personne("Fincher","David");
		p.save();
		
		Film.createTable();
		
		Film f=new Film("Arche perdue",Personne.findById(1));
		f.save();
		
		f=new Film("Alien",Personne.findById(2));
		f.save();
		
		f=new Film("Temple Maudit",Personne.findById(1));
		f.save();
		
		f=new Film("Blade Runner",Personne.findById(1));
		f.save();
		

	}
		
	
	

	/**
	 * Reinitialisation temporaire dans la base de donn�es pour effectuer des testes
	 * @throws SQLException
	 */
	@After 
	public void reinitilisation() throws SQLException {
		Personne.deleteTable();
		Film.deleteTable();
		
	}
	
	/**
	 * Test de findByAll
	 * on v�rifiant tous les param�tres avec ceux de la base de donn�es
	 * @throws SQLException
	 */
	@Test
	public void findByAll_test() throws SQLException {
		String SQLPrep = "SELECT * FROM Film;";
		Connection connect = DBConnection.getInstance().getConnect();
		PreparedStatement prep1 = connect.prepareStatement(SQLPrep);
		prep1.execute();
		ResultSet rs = prep1.getResultSet();
		List<Film> list=new ArrayList<Film>();
		while (rs.next()) {
			String titre = rs.getString("TITRE");
			
			int id = rs.getInt("ID");
			int id_real= rs.getInt("ID_REA");
			Film f= new Film(titre,Personne.findById(id_real));
			f.setId(id);
			list.add(f);
		}
		for(int i=0;i<list.size();i++) {
			assertEquals("�a devrait etre le meme",list.get(i).getId(),(Film.findAll().get(i).getId()));
			assertEquals("�a devrait etre le meme",list.get(i).getTitre(),(Film.findAll().get(i).getTitre()));
			assertEquals("�a devrait etre le meme",list.get(i).getRealisateur().getId(),(Film.findAll().get(i).getRealisateur().getId()));
			assertEquals("�a devrait etre le meme",list.get(i).getRealisateur().getNom(),(Film.findAll().get(i).getRealisateur().getNom()));
			assertEquals("�a devrait etre le meme",list.get(i).getRealisateur().getPrenom(),(Film.findAll().get(i).getRealisateur().getPrenom()));
		}
	
		
		
	}
	
	/**
	 * @throws SQLException
	 */
	@Test
	public void findById_test() throws SQLException {
		
		Personne p =new Personne("SpielBeg","Steven");
		p.setId(1);
		
		assertEquals("�a devrait etre le meme",p.getNom(),Personne.findById(1).getNom());
		assertEquals("�a devrait etre le meme",p.getPrenom(),Personne.findById(1).getPrenom());
		assertEquals("�a devrait etre le meme",p.getId(),Personne.findById(1).getId());
	
	}

	/**
	 * @throws SQLException
	 * @throws RealisateurAbsentException
	 */
	@Test
	public void save_test() throws SQLException, RealisateurAbsentException {
		Personne p=Personne.findById(2);
		Film f=new Film("Test",p);
		f.save();
		assertEquals("",Film.findByRealisateur(p).get(1).getTitre(),f.getTitre());
		
		
		f=new Film("Test",Personne.findById(2));
		f.setId(1);
		f.save();
		assertEquals("�a devrait etre le meme",Film.findById(1).getTitre(),f.getTitre());
		assertEquals("�a devrait etre le meme",Film.findById(1).getId_real(),f.getId_real());
		
		
	}
	
	/**
	 * @throws SQLException
	 */
	@Test
	public void delete_test() throws SQLException {
		Film f=Film.findById(1);
		f.delete();
		assertEquals("�a devrait etre �gal � -1",-1,f.getId());
		
		
	}
	
	/**
	 * @throws SQLException
	 */
	@Test
	public void findByRealisateur_test() throws SQLException {
		
		assertEquals("�a devrait etre le meme",( Film.findByRealisateur(Personne.findById(2))).get(0).getTitre(),"Alien");
		assertEquals("�a devrait etre le meme",( Film.findByRealisateur(Personne.findById(2))).get(0).getId_real(),Personne.findById(2).getId());
		
	}
}
