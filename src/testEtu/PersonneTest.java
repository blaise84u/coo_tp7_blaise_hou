package testEtu;

import static org.junit.Assert.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import activeRecord.DBConnection;
import activeRecord.Personne;



public class PersonneTest {
	
	/**
	 * @throws SQLException
	 */
	@Before
	public void init() throws SQLException {
		Personne.createTable();
		
		Personne p=new Personne("SpielBeg","Steven");
		p.save();
		
		p=new Personne("Scott","Ridley");
		p.save();
		
		p=new Personne("Kubrick","Stanley");
		p.save();
		
		p=new Personne("Fincher","David");
		p.save();
		
		
		
	}
	

	/**
	 * @throws SQLException
	 */
	@After 
	public void reinitilisation() throws SQLException {
		Personne.deleteTable();
	
	}
	
	
	/**
	 * @throws SQLException
	 */
	@Test
	public void findByAll_test() throws SQLException {
		
	
		String SQLPrep = "SELECT * FROM Personne;";
		Connection connect = DBConnection.getInstance().getConnect();
		PreparedStatement prep1 = connect.prepareStatement(SQLPrep);
		prep1.execute();
		ResultSet rs = prep1.getResultSet();
		List<Personne> list=new ArrayList<Personne>();
		while (rs.next()) {
			String nom = rs.getString("nom");
			String prenom = rs.getString("prenom");
			int id = rs.getInt("id");
			
			Personne p= new Personne(nom,prenom);
			p.setId(id);
			list.add(p);
		}
		for(int i=0;i<list.size();i++) {
			assertEquals("�a devrait etre le meme",list.get(i).getId(),(Personne.findAll().get(i).getId()));
			assertEquals("�a devrait etre le meme",list.get(i).getNom(),(Personne.findAll().get(i).getNom()));
			assertEquals("�a devrait etre le meme",list.get(i).getPrenom(),(Personne.findAll().get(i).getPrenom()));
		}
		
	}
	
	/**
	 * @throws SQLException
	 */
	@Test
	public void findById_test() throws SQLException {
		
		Personne p =new Personne("SpielBeg","Steven");
		p.setId(1);
		
		assertEquals("�a devrait etre le meme",p.getNom(),Personne.findById(1).getNom());
		assertEquals("�a devrait etre le meme",p.getPrenom(),Personne.findById(1).getPrenom());
		assertEquals("�a devrait etre le meme",p.getId(),Personne.findById(1).getId());
	}

	/**
	 * @throws SQLException
	 */
	@Test
	public void findByName_test() throws SQLException {
		
		Personne p =new Personne("SpielBeg","Steven");
		p.setId(1);
		
		assertEquals("�a devrait etre le meme",Personne.findByName("SpielBeg").get(0).getNom(),p.getNom());
		assertEquals("�a devrait etre le meme",Personne.findByName("SpielBeg").get(0).getPrenom(),p.getPrenom());
		assertEquals("�a devrait etre le meme",Personne.findByName("SpielBeg").get(0).getId(),p.getId());
	}
	

	
	/**
	 * @throws SQLException
	 */
	@Test
	public void save_test() throws SQLException {
		Personne p=new Personne("Test","Test");
		p.save();
		assertEquals("�a devrait etre le meme",Personne.findByName("Test").get(0).getNom(),p.getNom());
		assertEquals("�a devrait etre le meme",Personne.findByName("Test").get(0).getPrenom(),p.getPrenom());
		assertEquals("�a devrait etre le meme",Personne.findByName("Test").get(0).getId(),5);
		
		p=new Personne("Test","Test");
		p.setId(1);
		p.save();
		assertEquals("�a devrait etre le meme",Personne.findById(1).getNom(),p.getNom());
		assertEquals("�a devrait etre le meme",Personne.findById(1).getPrenom(),p.getPrenom());
		assertEquals("�a devrait etre le meme",Personne.findById(1).getId(),1);
		
	}
	
	/**
	 * @throws SQLException
	 */
	@Test
	public void delete_test() throws SQLException {
		Personne p=Personne.findById(1);
		p.delete();
		assertEquals("�a devrait etre �gal � -1",-1,p.getId());
		
	}
	
	
	
	
	




}
