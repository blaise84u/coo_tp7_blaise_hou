package testEtu;

import static org.junit.Assert.*;

import org.junit.Test;

import activeRecord.DBConnection;

import java.sql.Connection;
import java.sql.SQLException;


public class DBConnectionTest {
	/*
	 *Test de la methode getConnection()
	 *en verifiant que 2 connections sont la meme
	 *@throws SQLException
	 */
	@Test
	public void testGetConnection() throws SQLException {
		Connection c1 = DBConnection.getInstance().getConnect();
		Connection c2 = DBConnection.getInstance().getConnect();
		
		assertEquals("�a devrait etre le meme",c1,c2);
		assertTrue("�a ne devrait pas etre null", c1 != null);
		assertTrue("�a devrait pas etre null",c2 != null);
		DBConnection.setNomDB("testsetnomdb");
		c2= DBConnection.getInstance().getConnect();
		assertFalse("�a devrait pas etre �gal � c1",c2 == c1);
		
	}

}
